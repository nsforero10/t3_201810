package model.data_structures;


public class DobleNode <T>
{
	private T element;
	private DobleNode<T> prev;
	private DobleNode<T> next;

	public DobleNode(T e, DobleNode<T> p, DobleNode<T> n)
	{
		element = e;
		prev = p;
		next = n;
	}

	public T getElement()
	{
		return element;
	}

	public void setElement(T e)
	{
		element = e;
	}

	public DobleNode<T> getPrev()
	{
		return prev;
	}

	public DobleNode<T> getNext()
	{
		return next;
	}

	public void setPrev(DobleNode<T> p)
	{
		prev = p;
	}

	public void setNext(DobleNode<T> n)
	{
		next = n;
	}

}
