package model.data_structures;


public class SimpleNode<T>
{
	private T element;

	private SimpleNode<T> next;

	public SimpleNode(T e, SimpleNode<T>n)
	{
		element = e;
		next = n;
	}

	public T getElement()
	{
		return element;
	}

	public void setElement(T element)
	{
		this.element = element;
	}

	public SimpleNode<T> getNext()
	{
		return next;
	}

	public void setNext(SimpleNode<T> next)
	{
		this.next = next;
	}

}